*** Settings ***
Resource            ${CURDIR}/../pages/login.robot

*** Keywords ***
Sign In Website
    [Arguments]         ${username}     ${password}
    login.Input Username  ${username}
    login.Input Password  ${password}
    login.Click Button To Sign In