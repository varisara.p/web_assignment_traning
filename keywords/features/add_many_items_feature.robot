*** Settings ***
Resource        ${CURDIR}/../pages/cart.robot

*** Keywords ***
Add How Many Items You Want 
    [Arguments]         ${input_value}
    cart.Wait Page Item Response
    FOR     ${i}     IN RANGE   1   ${input_value}  
        cart.Add Number Of Items
    END