*** Settings ***
Resource        ${CURDIR}/../pages/cart.robot

*** Keywords ***
Check Number Correctly Items In Cart
    [Arguments]         ${input_value}
    Sleep               1
    cart.Check Number of Items In Cart  ${input_value}