*** Settings ***
Resource        ${CURDIR}/../pages/searchBar.robot

*** Keywords ***
Search Item 
    [Arguments]             ${input_value}
    searchBar.Input Value To Search   ${input_value}
    searchBar.Click To Search