*** Settings ***
Resource        ${CURDIR}/../pages/searchResult.robot

*** Keywords ***
Verify Search Found Nothing 
    searchResult.Wait Page Search Result Response
    searchResult.Search Found Nothing 