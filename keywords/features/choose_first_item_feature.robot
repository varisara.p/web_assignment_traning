*** Settings ***
Library         SeleniumLibrary

*** Variables ***
${product_locator}          id=product-card

*** Keywords ***
Wait And Select First Item
    Wait Until Element Is Visible       ${product_locator}
    Click Element                       ${product_locator}