*** Settings ***
Resource        ${CURDIR}/../pages/searchResult.robot

*** Keywords ***
Verify Search Found  
    [Arguments]         ${input_value}
    searchResult.Wait Page Search Result Response
    searchResult.Search Found Result  ${input_value}