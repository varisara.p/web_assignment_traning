*** Settings ***
Library             SeleniumLibrary

*** Keywords ***
Wait Page Search Result Response
    Wait Until Page Contains         Search Result

Search Found Nothing 
    Element Should Contain          id=notFound             No results found.

Search Found Result
    [Arguments]             ${input_value}
    Wait Until Element Is Visible       //div[@class='column is-2']
    Element Should Be Visible          //p[contains(text(),'${input_value}')]   