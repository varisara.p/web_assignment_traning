*** Settings ***
Library             SeleniumLibrary

*** Variables ***
${username_locator}     id=username
${password_locator}     id=password
${button_locator}       id=loginbtn

*** Keywords ***
Input Username
    [Arguments]         ${input_value}
    Input Text          ${username_locator}     ${input_value}

Input Password
    [Arguments]         ${input_value}
    Input Text          ${password_locator}     ${input_value}

Click Button To Sign In
    Click Button        ${button_locator} 