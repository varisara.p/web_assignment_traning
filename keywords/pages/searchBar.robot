*** Settings ***
Library         SeleniumLibrary

*** Variables ***
${search_locator}               id=keyword
${search_button_locator}        id=searchbtn

*** Keywords ***
Input Value To Search
    [Arguments]         ${input_value}
    Wait Until Element Is Visible   ${search_locator}
    Input Text          ${search_locator}       ${input_value}

Click To Search
    Click Button        ${search_button_locator} 