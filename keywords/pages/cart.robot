*** Settings ***
Library         SeleniumLibrary

*** Variables ***
${addButton_locator}        xpath=//button[text()='Add To Cart']
${addNumberItem_locator}    xpath=//button[text()='+']
${clearCart_locator}        xpath=//button[text()='Clear All Cart']
${numberOfItem_locator}     id=lblCartCount
${numberItemIncrease_locator}       id=qty
${cart_locator}             xpath=//i[@class="fas fa-shopping-cart fa-2x"]

*** Keywords ***
Wait Page Item Response
    Wait Until Element is Visible   ${addButton_locator}

Add To Cart
    Click Button        ${addButton_locator}

Add Number Of Items    
    Wait Until Element is Visible   ${addNumberItem_locator}  
    Click Button        ${addNumberItem_locator}

Clear Cart
    Wait Until Element is Visible   ${cart_locator}
    Click Element       ${cart_locator}
    Wait Until Element is Visible   ${clearCart_locator}
    Click Button        ${clearCart_locator}

Check Number of Items In Cart
    [Arguments]         ${input_value}
    Wait Until Element is Visible   ${numberOfItem_locator}
    Element Should Contain      ${numberOfItem_locator}         ${input_value}
    