*** Settings ***
Library         SeleniumLibrary

Resource        ${CURDIR}/keywords/features/open_website_feature.robot
Resource        ${CURDIR}/keywords/features/login_feature.robot
Resource        ${CURDIR}/keywords/features/fail_login_feature.robot
Resource        ${CURDIR}/keywords/features/success_login_feature.robot
Resource        ${CURDIR}/keywords/features/search_feature.robot
Resource        ${CURDIR}/keywords/features/search_notFound_feature.robot
Resource        ${CURDIR}/keywords/features/search_found_feature.robot 
Resource        ${CURDIR}/keywords/features/choose_first_item_feature.robot 
Resource        ${CURDIR}/keywords/features/add_item_feature.robot 
Resource        ${CURDIR}/keywords/features/check_number_item_feature.robot
Resource        ${CURDIR}/keywords/features/add_many_items_feature.robot