*** Settings ***
Resource        ${CURDIR}/../import.robot

*** Test Cases ***
Testcase 01 - Verify that user cannot login with wrong username
    Open Browser To Test
    Sign In Website  username  1234
    Check Aleart That Worng Sign In
    Close Browser

Testcase 02 - Verify that user can login to the system when username are correct
    Open Browser To Test
    Sign In Website   oz4899    1234
    Check That Sign In Success And It Bring To Home Page