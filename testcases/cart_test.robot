*** Settings ***
Resource        ${CURDIR}/../import.robot

*** Test Cases ***
Testcase 04 - Verify that add to cart function are working correctly
    Open Browser To Test
    Sign In Website  oz4899  1234
    Wait And Select First Item 
    Add Item To Cart
    Check Number Correctly Items In Cart    1
    Add How Many Items You Want             5     
    Add Item To Cart
    Check Number Correctly Items In Cart    6
    Clear Cart
    Check Number Correctly Items In Cart    0
    