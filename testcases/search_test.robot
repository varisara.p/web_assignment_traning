*** Settings ***
Resource        ${CURDIR}/../import.robot

*** Test Cases ***
Testcase 03 - Verify that search function are working correctly
    Open Browser To Test
    Sign In Website  oz4899  1234
    Search Item         abcd
    Verify Search Found Nothing 
    Search Item         Shirt
    Verify Search Found  Shirt